import 'package:flutter_test/flutter_test.dart';
import 'package:relatica/friendica_client/paged_response.dart';
import 'package:relatica/friendica_client/paging_data.dart';

void main() {
  const data = 'Hello';
  group('Test fromLinkHeader', () {
    test('Null header (as if not there)', () {
      expect(
        PagedResponse.fromLinkHeader(null, data).value,
        equals(PagedResponse(data)),
      );
    });

    test('Empty header', () {
      expect(
        PagedResponse.fromLinkHeader('', data).value,
        equals(PagedResponse(data)),
      );
    });

    test('Not a previous/next header', () {
      expect(
        PagedResponse.fromLinkHeader(
          '<https://example.com>; rel="preconnect"',
          data,
        ).value,
        equals(PagedResponse(data)),
      );
    });

    test('Previous and next', () {
      expect(
        PagedResponse.fromLinkHeader(
          '<https://friendica.myportal.social/api/v1/accounts/1/followers?max_id=550>; rel="next", <https://friendica.myportal.social/api/v1/accounts/1/followers?min_id=590>; rel="prev"',
          data,
        ).value,
        equals(PagedResponse(
          data,
          previous: PagingData(minId: 590),
          next: PagingData(maxId: 550),
        )),
      );
    });
  });

  test('Test Mapping', () {
    final original = PagedResponse(data,
        previous: PagingData(minId: 2), next: PagingData(maxId: 3));
    expect(
        original.map((data) => data.length),
        equals(PagedResponse(data.length,
            previous: PagingData(minId: 2), next: PagingData(maxId: 3))));
  });
}
