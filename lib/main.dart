import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:multi_trigger_autocomplete/multi_trigger_autocomplete.dart';
import 'package:provider/provider.dart';

import 'app_theme.dart';
import 'di_initialization.dart';
import 'globals.dart';
import 'routes.dart';
import 'services/auth_service.dart';
import 'services/connections_manager.dart';
import 'services/direct_message_service.dart';
import 'services/entry_manager_service.dart';
import 'services/gallery_service.dart';
import 'services/hashtag_service.dart';
import 'services/notifications_manager.dart';
import 'services/setting_service.dart';
import 'services/timeline_manager.dart';
import 'utils/app_scrolling_behavior.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await dotenv.load(fileName: '.env');
  Logger.root.level = Level.FINER;

  Logger.root.onRecord.listen((event) {
    final logName = event.loggerName.isEmpty ? 'ROOT' : event.loggerName;
    final msg =
        '${event.level.name} - $logName @ ${event.time}: ${event.message}';
    print(msg);
  });

  await dependencyInjectionInitialization();

  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: (context, child) {
        return Portal(
          child: MultiProvider(
            providers: [
              ChangeNotifierProvider<SettingsService>(
                create: (_) => getIt<SettingsService>(),
                lazy: true,
              ),
              ChangeNotifierProvider<AuthService>(
                create: (_) => getIt<AuthService>(),
                lazy: true,
              ),
              ChangeNotifierProvider<ConnectionsManager>(
                create: (_) => getIt<ConnectionsManager>(),
                lazy: true,
              ),
              ChangeNotifierProvider<EntryManagerService>(
                create: (_) => getIt<EntryManagerService>(),
                lazy: true,
              ),
              ChangeNotifierProvider<GalleryService>(
                create: (_) => getIt<GalleryService>(),
                lazy: true,
              ),
              ChangeNotifierProvider<HashtagService>(
                create: (_) => getIt<HashtagService>(),
                lazy: true,
              ),
              ChangeNotifierProvider<TimelineManager>(
                create: (_) => getIt<TimelineManager>(),
              ),
              ChangeNotifierProvider<NotificationsManager>(
                create: (_) => getIt<NotificationsManager>(),
              ),
              ChangeNotifierProvider<DirectMessageService>(
                create: (_) => getIt<DirectMessageService>(),
              )
            ],
            child: MaterialApp.router(
              theme: AppTheme.light,
              darkTheme: AppTheme.dark,
              themeMode: getIt<SettingsService>().themeMode,
              debugShowCheckedModeBanner: false,
              scrollBehavior: AppScrollingBehavior(),
              routerDelegate: appRouter.routerDelegate,
              routeInformationProvider: appRouter.routeInformationProvider,
              routeInformationParser: appRouter.routeInformationParser,
            ),
          ),
        );
      },
      animation: getIt<SettingsService>(),
    );
  }
}
