class DirectMessage {
  final String id;

  final String senderId;

  final String senderScreenName;

  final String recipientId;

  final String recipientScreenName;

  final String title;

  final String text;

  final int createdAt;

  final bool seen;

  final String parentUri;

  DirectMessage({
    required this.id,
    required this.senderId,
    required this.senderScreenName,
    required this.recipientId,
    required this.recipientScreenName,
    required this.title,
    required this.text,
    required this.createdAt,
    required this.seen,
    required this.parentUri,
  });

  DirectMessage copy({
    String? id,
    String? senderId,
    String? senderScreenName,
    String? recipientId,
    String? recipientScreenName,
    String? title,
    String? text,
    int? createdAt,
    bool? seen,
    String? parentUri,
  }) =>
      DirectMessage(
        id: id ?? this.id,
        senderId: senderId ?? this.senderId,
        senderScreenName: senderScreenName ?? this.senderScreenName,
        recipientId: recipientId ?? this.recipientId,
        recipientScreenName: recipientScreenName ?? this.recipientScreenName,
        title: title ?? this.title,
        text: text ?? this.text,
        createdAt: createdAt ?? this.createdAt,
        seen: seen ?? this.seen,
        parentUri: parentUri ?? this.parentUri,
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DirectMessage &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;
}
