class GroupData {
  final String id;
  final String name;

  GroupData(this.id, this.name);

  @override
  String toString() {
    return 'GroupData{id: $id, name: $name}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GroupData && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}
