import 'package:objectbox/objectbox.dart';

@Entity()
class Hashtag {
  @Id()
  int id;

  @Unique(onConflict: ConflictStrategy.replace)
  String tag;

  String url;

  @Property(type: PropertyType.date)
  DateTime lastUpdateTime;

  Hashtag({
    this.id = 0,
    required this.tag,
    required this.url,
    DateTime? updateTime,
  }) : lastUpdateTime = updateTime ?? DateTime.now();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Hashtag && runtimeType == other.runtimeType && tag == other.tag;

  @override
  int get hashCode => tag.hashCode;
}
