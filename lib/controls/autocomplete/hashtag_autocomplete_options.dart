import 'package:flutter/material.dart';

import '../../globals.dart';
import '../../services/hashtag_service.dart';

class HashtagAutocompleteOptions extends StatelessWidget {
  const HashtagAutocompleteOptions({
    Key? key,
    required this.query,
    required this.onHashtagTap,
  }) : super(key: key);

  final String query;
  final ValueSetter<String> onHashtagTap;

  @override
  Widget build(BuildContext context) {
    final hashtags = getIt<HashtagService>().getMatchingHashTags(query);

    if (hashtags.isEmpty) return const SizedBox.shrink();

    return Card(
      margin: const EdgeInsets.all(8),
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      clipBehavior: Clip.hardEdge,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            color: const Color(0xFFF7F7F8),
            child: ListTile(
              dense: true,
              horizontalTitleGap: 0,
              title: Text("Hashtags matching '$query'"),
            ),
          ),
          LimitedBox(
            maxHeight: MediaQuery.of(context).size.height * 0.2,
            child: ListView.separated(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: hashtags.length,
              separatorBuilder: (_, __) => const Divider(),
              itemBuilder: (context, i) {
                final hashtag = hashtags[i];
                return GestureDetector(
                  onTap: () => onHashtagTap(hashtag),
                  child: Text(hashtag),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
