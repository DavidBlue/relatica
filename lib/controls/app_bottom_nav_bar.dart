import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../routes.dart';
import '../services/notifications_manager.dart';

enum NavBarButtons {
  timelines,
  notifications,
  contacts,
  menu,
}

class AppBottomNavBar extends StatelessWidget {
  final NavBarButtons currentButton;

  const AppBottomNavBar({super.key, required this.currentButton});

  @override
  Widget build(BuildContext context) {
    final notificationManager = context.watch<NotificationsManager>();
    final hasNotifications =
        notificationManager.notifications.where((n) => !n.dismissed).isNotEmpty;
    return BottomNavigationBar(
      onTap: (index) {
        final newButton = _indexToButton(index);
        if (newButton == currentButton) {
          return;
        }

        switch (newButton) {
          case NavBarButtons.timelines:
            Navigator.of(context).popUntil((route) {
              return route.settings.name == ScreenPaths.timelines;
            });
            break;
          case NavBarButtons.notifications:
            context.pushNamed(ScreenPaths.notifications);
            break;
          case NavBarButtons.contacts:
            context.pushNamed(ScreenPaths.contacts);
            break;
          case NavBarButtons.menu:
            context.pushNamed(ScreenPaths.menu);
            break;
        }
      },
      type: BottomNavigationBarType.fixed,
      currentIndex: _buttonToIndex(currentButton),
      items: _menuItems(context, hasNotifications),
    );
  }

  int _buttonToIndex(NavBarButtons button) {
    switch (button) {
      case NavBarButtons.timelines:
        return 0;
      case NavBarButtons.notifications:
        return 1;
      case NavBarButtons.contacts:
        return 2;
      case NavBarButtons.menu:
        return 3;
    }
  }

  NavBarButtons _indexToButton(int index) {
    if (index == 0) {
      return NavBarButtons.timelines;
    }

    if (index == 1) {
      return NavBarButtons.notifications;
    }

    if (index == 2) {
      return NavBarButtons.contacts;
    }

    if (index == 3) {
      return NavBarButtons.menu;
    }

    throw ArgumentError('$index has no button type');
  }

  List<BottomNavigationBarItem> _menuItems(
      BuildContext context, bool hasNotifications) {
    return [
      const BottomNavigationBarItem(
        label: 'Timelines',
        icon: Icon(Icons.home_outlined),
        activeIcon: Icon(Icons.home_filled),
      ),
      BottomNavigationBarItem(
        label: 'Notifications',
        icon: Icon(hasNotifications
            ? Icons.notifications_active_outlined
            : Icons.notifications_none_outlined),
        activeIcon: Icon(hasNotifications
            ? Icons.notifications_active
            : Icons.notifications),
      ),
      const BottomNavigationBarItem(
        label: 'Contacts',
        icon: Icon(Icons.people_outline),
        activeIcon: Icon(Icons.people_sharp),
      ),
      const BottomNavigationBarItem(
        label: 'Menu',
        icon: Icon(Icons.menu),
        activeIcon: Icon(Icons.menu_open),
      ),
    ];
  }
}
