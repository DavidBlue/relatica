import 'package:flutter/foundation.dart';
import 'package:result_monad/result_monad.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../friendica_client/friendica_client.dart';
import '../globals.dart';
import '../models/credentials.dart';
import '../models/exec_error.dart';
import 'secrets_service.dart';
import 'timeline_manager.dart';

class AuthService extends ChangeNotifier {
  FriendicaClient? _friendicaClient;
  String _currentId = '';
  String _currentHandle = '';
  String _currentServer = '';
  String _currentAvatarUrl = '';
  bool _loggedIn = false;

  Result<FriendicaClient, ExecError> get currentClient {
    if (_friendicaClient == null) {
      return Result.error(ExecError(
        type: ErrorType.authentication,
        message: 'Not logged in',
      ));
    }

    return Result.ok(_friendicaClient!);
  }

  bool get loggedIn => _loggedIn && _friendicaClient != null;

  String get currentId => _currentId;

  String get currentHandle => _currentHandle;

  String get currentServer => _currentServer;

  String get currentAvatarUrl => _currentAvatarUrl;

  Future<bool> getStoredLoginState() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool('logged-in') ?? false;
  }

  FutureResult<FriendicaClient, ExecError> signIn(
      Credentials credentials) async {
    final client = FriendicaClient(credentials: credentials);
    final result = await client.getMyProfile();
    if (result.isFailure) {
      return result.errorCast();
    }

    getIt<SecretsService>().storeCredentials(client.credentials);
    await _setLoginState(true);
    _friendicaClient = client;
    _currentId = result.value.id;
    _currentHandle = client.credentials.handle;
    _currentServer = client.credentials.serverName;
    _currentAvatarUrl = result.value.avatarUrl;
    notifyListeners();
    return Result.ok(client);
  }

  Future signOut() async {
    await _setLoginState(false);
    getIt<TimelineManager>().clear();
    _friendicaClient = null;
    _currentId = '';
    _currentHandle = '';
    _currentServer = '';
    notifyListeners();
  }

  Future clearCredentials() async {
    _friendicaClient = null;
    await _setLoginState(false);
    notifyListeners();
  }

  Future<void> _setLoginState(bool state) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('logged-in', state);
    _loggedIn = state;
  }
}
