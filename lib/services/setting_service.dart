import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/theme_mode_extensions.dart';

class SettingsService extends ChangeNotifier {
  late final SharedPreferences _prefs;
  var _initialized = false;

  SettingsService();

  var _lowBandwidthMode = false;

  bool get lowBandwidthMode => _lowBandwidthMode;

  set lowBandwidthMode(bool value) {
    _lowBandwidthMode = value;
    _prefs.setBool(_lowBandwidthModeKey, _lowBandwidthMode);
    notifyListeners();
  }

  var _themeMode = ThemeMode.system;

  ThemeMode get themeMode => _themeMode;

  set themeMode(ThemeMode mode) {
    _themeMode = mode;
    _prefs.setString(_themeModeKey, _themeMode.name);
    notifyListeners();
  }

  Future<void> initialize() async {
    if (_initialized) {
      return;
    }
    _prefs = await SharedPreferences.getInstance();
    _lowBandwidthMode = _prefs.getBool(_lowBandwidthModeKey) ?? false;
    _themeMode = ThemeModeExtensions.parse(_prefs.getString(_themeModeKey));
    _initialized = true;
  }
}

const _lowBandwidthModeKey = 'LowBandwidthMode';
const _themeModeKey = 'ThemeMode';
