import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:result_monad/result_monad.dart';

import '../models/credentials.dart';
import '../models/exec_error.dart';

class SecretsService {
  static const _usernameKey = 'username';
  static const _passwordKey = 'password';
  static const _serverNameKey = 'server-name';

  Credentials? _cachedCredentials;

  Result<Credentials, ExecError> get credentials => _cachedCredentials != null
      ? Result.ok(_cachedCredentials!)
      : Result.error(
          ExecError(
            type: ErrorType.localError,
            message: 'Credentials not initialized',
          ),
        );

  final _secureStorage = const FlutterSecureStorage(
    iOptions: IOSOptions(
      accessibility: KeychainAccessibility.first_unlock,
    ),
  );

  FutureResult<Credentials, ExecError> initialize() async {
    return await getCredentials();
  }

  FutureResult<Credentials, ExecError> clearCredentials() async {
    try {
      await _secureStorage.delete(key: _usernameKey);
      await _secureStorage.read(key: _passwordKey);
      await _secureStorage.read(key: _serverNameKey);
      return Result.ok(Credentials.empty());
    } on PlatformException catch (e) {
      return Result.error(ExecError(
        type: ErrorType.localError,
        message: e.message ?? '',
      ));
    }
  }

  Result<Credentials, ExecError> storeCredentials(Credentials credentials) {
    try {
      _secureStorage.write(key: _usernameKey, value: credentials.username);
      _secureStorage.write(key: _passwordKey, value: credentials.password);
      _secureStorage.write(key: _serverNameKey, value: credentials.serverName);
      _cachedCredentials = credentials;
      return Result.ok(credentials);
    } on PlatformException catch (e) {
      return Result.error(ExecError(
        type: ErrorType.localError,
        message: e.message ?? '',
      ));
    }
  }

  FutureResult<Credentials, ExecError> getCredentials() async {
    try {
      final username = await _secureStorage.read(key: _usernameKey);
      final password = await _secureStorage.read(key: _passwordKey);
      final serverName = await _secureStorage.read(key: _serverNameKey);
      if (username == null || password == null || serverName == null) {
        return Result.ok(Credentials.empty());
      }
      _cachedCredentials = Credentials(
        username: username,
        password: password,
        serverName: serverName,
      );
      return Result.ok(_cachedCredentials!);
    } on PlatformException catch (e) {
      return Result.error(ExecError(
        type: ErrorType.localError,
        message: e.message ?? '',
      ));
    }
  }
}
