extension StringUtils on String {
  String truncate({int length = 32}) {
    if (this.length <= length) {
      return this;
    }

    return '${substring(0, length)}...';
  }
}
