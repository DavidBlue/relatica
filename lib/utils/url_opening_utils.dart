import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

import 'snackbar_builder.dart';

Future<bool> openUrlStringInSystembrowser(
    BuildContext context, String url, String label) async {
  final uri = Uri.tryParse(url);
  if (uri == null) {
    buildSnackbar(context, 'Bad link: $url');
    return false;
  }
  if (await canLaunchUrl(uri)) {
    buildSnackbar(
      context,
      'Attempting to launch $label: $url',
    );
    await launchUrl(uri);
  } else {
    buildSnackbar(context, 'Unable to launch $label: $url');
    return false;
  }
  return true;
}
