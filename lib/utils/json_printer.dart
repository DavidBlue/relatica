import 'dart:convert';

class PrettyJsonEncoder {
  late JsonEncoder encoder;

  PrettyJsonEncoder() {
    encoder = JsonEncoder.withIndent('\t');
  }

  String convert(Object json) => encoder.convert(json);
}
