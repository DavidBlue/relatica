import 'package:logging/logging.dart';
import 'package:objectbox/objectbox.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

import '../../objectbox.g.dart';

class ObjectBoxCache {
  static final _logger = Logger('ObjectBoxCache');
  late final Store store;

  ObjectBoxCache._create(this.store);

  static Future<ObjectBoxCache> create() async {
    final docsDir = await getApplicationSupportDirectory();

    final path = p.join(docsDir.path, 'objectboxcache');
    _logger.info('ObjectBoxCache path: $path');
    final store =
        await openStore(directory: path, macosApplicationGroup: 'T69YZGT58U.relatica');
    return ObjectBoxCache._create(store);
  }
}
