import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import '../controls/padding.dart';
import '../controls/standard_appbar.dart';
import '../serializers/friendica/image_entry_friendica_extensions.dart';
import '../services/gallery_service.dart';
import 'image_viewer_screen.dart';

class GalleryScreen extends StatelessWidget {
  static const thumbnailDimension = 350.0;
  static final _logger = Logger('$GalleryScreen');
  final String galleryName;

  const GalleryScreen({super.key, required this.galleryName});

  @override
  Widget build(BuildContext context) {
    _logger.finest('Building');
    final service = context.watch<GalleryService>();
    return Scaffold(
      appBar: StandardAppBar.build(context, galleryName),
      body: RefreshIndicator(
          onRefresh: () async {
            await service.updateGalleryImageList(galleryName);
          },
          child: buildBody(context, service)),
    );
  }

  Widget buildBody(BuildContext context, GalleryService service) {
    final imageResult = service.getGalleryImageList(galleryName);
    if (imageResult.isFailure) {
      return SingleChildScrollView(
        child: Center(
          child: Text('Error getting images for gallery: ${imageResult.error}'),
        ),
      );
    }

    final images = imageResult.value;
    if (images.isEmpty && service.loaded) {
      return const SingleChildScrollView(
        child: Center(
          child: Text('No images'),
        ),
      );
    }

    if (images.isEmpty) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Text('Loading images'),
            VerticalPadding(),
            CircularProgressIndicator(),
          ],
        ),
      );
    }

    return GridView.builder(
        itemCount: images.length,
        padding: EdgeInsets.all(5.0),
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: thumbnailDimension),
        itemBuilder: (context, index) {
          final image = images[index];
          return Padding(
            padding: const EdgeInsets.all(2.0),
            child: GestureDetector(
              onDoubleTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ImageViewerScreen(
                      attachment: image.toMediaAttachment());
                }));
              },
              child: CachedNetworkImage(
                width: thumbnailDimension,
                height: thumbnailDimension,
                imageUrl: image.thumbnailUrl,
              ),
            ),
          );
        });
  }
}
