import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../controls/image_control.dart';
import '../controls/standard_appbar.dart';
import '../routes.dart';
import '../services/direct_message_service.dart';
import '../utils/dateutils.dart';

class MessagesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final service = context.watch<DirectMessageService>();
    return Scaffold(
      appBar: StandardAppBar.build(context, 'Direct Message Threads', actions: [
        IconButton(
          onPressed: () {
            context.push('/messages/new_thread');
          },
          icon: Icon(Icons.add),
        )
      ]),
      body: RefreshIndicator(
        onRefresh: () async {
          await service.updateThreads();
        },
        child: Center(child: buildBody(context, service)),
      ),
    );
  }

  Widget buildBody(BuildContext context, DirectMessageService service) {
    final threads = service.threads;
    threads.sort((t1, t2) =>
        t2.messages.last.createdAt.compareTo(t1.messages.last.createdAt));
    return threads.isEmpty
        ? const Text('No Direct Message Threads')
        : ListView.separated(
            itemCount: threads.length,
            itemBuilder: (context, index) {
              final thread = threads[index];
              final style = thread.allSeen
                  ? null
                  : TextStyle(fontWeight: FontWeight.bold);
              return ListTile(
                onTap: () => context.pushNamed(
                  ScreenPaths.thread,
                  queryParams: {'uri': thread.parentUri},
                ),
                leading: ImageControl(
                  imageUrl: thread.participants.first.avatarUrl.toString(),
                  iconOverride: const Icon(Icons.person),
                  width: 32.0,
                  onTap: null,
                ),
                title: Text(
                  [
                    'You',
                    ...thread.participants.map((p) => '${p.name}(${p.handle})')
                  ].join(thread.participants.length == 1 ? ' & ' : ', '),
                  softWrap: true,
                  style: style,
                ),
                subtitle: Text(
                  thread.title,
                  style: style,
                ),
                trailing: Text(
                  ElapsedDateUtils.epochSecondsToString(
                      thread.messages.last.createdAt),
                  style: style,
                ),
              );
            },
            separatorBuilder: (_, __) => const Divider(),
          );
  }
}
