import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import '../controls/app_bottom_nav_bar.dart';
import '../models/connection.dart';
import '../routes.dart';
import '../services/connections_manager.dart';

class ContactsScreen extends StatefulWidget {
  @override
  State<ContactsScreen> createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen> {
  static final _logger = Logger('$ContactsScreen');
  var filterText = '';

  @override
  Widget build(BuildContext context) {
    final manager = context.watch<ConnectionsManager>();
    final allContacts = manager.getMyContacts();
    final filterTextLC = filterText.toLowerCase();
    final contacts = allContacts
        .where((c) =>
            filterText.isEmpty ||
            c.name.toLowerCase().contains(filterTextLC) ||
            c.handle.toLowerCase().contains(filterTextLC))
        .toList();
    contacts.sort((c1, c2) => c1.name.compareTo(c2.name));
    _logger.finer(
      () => '# Contacts: ${allContacts.length}, #filtered: ${contacts.length}',
    );
    late Widget body;
    if (contacts.isEmpty) {
      body = const SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Text('No Contacts'),
      );
    } else {
      body = ListView.separated(
          physics: const AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            final contact = contacts[index];
            return ListTile(
              onTap: () {
                context.pushNamed(ScreenPaths.userProfile,
                    params: {'id': contact.id});
              },
              title: Text(contact.name),
              trailing: Text(contact.status.label()),
            );
          },
          separatorBuilder: (context, index) => const Divider(),
          itemCount: contacts.length);
    }
    return Scaffold(
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () async {
            await manager.updateAllContacts();
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  onChanged: (value) {
                    setState(() {
                      filterText = value.toLowerCase();
                    });
                  },
                  decoration: InputDecoration(
                    labelText: 'Filter By Name',
                    alignLabelWithHint: true,
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Theme.of(context).backgroundColor,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: body,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: AppBottomNavBar(
        currentButton: NavBarButtons.contacts,
      ),
    );
  }
}
