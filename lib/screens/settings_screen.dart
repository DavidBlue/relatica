import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../controls/standard_appbar.dart';
import '../services/setting_service.dart';
import '../utils/theme_mode_extensions.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final settings = context.watch<SettingsService>();
    return Scaffold(
        appBar: StandardAppBar.build(context, 'Settings'),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: [
                buildLowBandwidthWidget(settings),
                buildThemeWidget(settings),
              ],
            ),
          ),
        ));
  }

  Widget buildLowBandwidthWidget(SettingsService settings) {
    return ListTile(
      title: const Text('Low bandwidth mode'),
      trailing: Checkbox(
        onChanged: (value) {
          settings.lowBandwidthMode = value ?? false;
        },
        value: settings.lowBandwidthMode,
      ),
    );
  }

  Widget buildThemeWidget(SettingsService settings) {
    return ListTile(
      title: const Text('Dark Mode Theme:'),
      trailing: DropdownButton<ThemeMode>(
        value: settings.themeMode,
        items: ThemeMode.values
            .map((m) => DropdownMenuItem(value: m, child: Text(m.toLabel())))
            .toList(),
        onChanged: (value) {
          if (value != null) {
            settings.themeMode = value;
          }
        },
      ),
    );
  }
}
