import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import '../controls/padding.dart';
import '../controls/standard_appbar.dart';
import '../services/gallery_service.dart';

class GalleryBrowsersScreen extends StatelessWidget {
  static final _logger = Logger('$GalleryBrowsersScreen');

  @override
  Widget build(BuildContext context) {
    _logger.finest('Building');
    final service = context.watch<GalleryService>();
    return Scaffold(
      appBar: StandardAppBar.build(context, 'Galleries'),
      body: RefreshIndicator(
        onRefresh: () async {
          await service.updateGalleries();
        },
        child: buildBody(context, service),
      ),
    );
  }

  Widget buildBody(BuildContext context, GalleryService service) {
    final galleries = service.getGalleries();

    if (galleries.isEmpty && service.loaded) {
      return const SingleChildScrollView(
        child: Center(
          child: Text('No Galleries'),
        ),
      );
    }

    if (galleries.isEmpty) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Text('Loading galleries'),
            VerticalPadding(),
            CircularProgressIndicator(),
          ],
        ),
      );
    }

    return ListView.separated(
      itemBuilder: (context, index) {
        final gallery = galleries[index];
        return InkWell(
          onTap: () {
            context.push('/gallery/show/${gallery.name}');
          },
          child: ListTile(
            title: Text(gallery.name),
            subtitle: Text(
              'Created: ${gallery.created}',
              style: Theme.of(context).textTheme.caption,
            ),
            trailing: Text('${gallery.count} Images'),
          ),
        );
      },
      separatorBuilder: (context, index) {
        return const Divider();
      },
      itemCount: galleries.length,
    );
  }
}
