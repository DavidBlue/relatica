import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../controls/app_bottom_nav_bar.dart';
import '../data/interfaces/connections_repo_intf.dart';
import '../globals.dart';
import '../routes.dart';
import '../services/auth_service.dart';
import '../services/notifications_manager.dart';
import '../services/timeline_manager.dart';

class MenusScreen extends StatelessWidget {
  static const menuButtonWidth = 350.0;
  static const menuButtonHeight = 125.0;

  @override
  Widget build(BuildContext context) {
    final menuItems = [
      buildMenuButton('Gallery', () => context.pushNamed(ScreenPaths.gallery)),
      buildMenuButton(
          'Direct Messages', () => context.pushNamed(ScreenPaths.messages)),
      buildMenuButton('Profile', () => context.pushNamed(ScreenPaths.profile)),
      buildMenuButton(
          'Settings', () => context.pushNamed(ScreenPaths.settings)),
      buildMenuButton('Clear Caches', () async {
        final confirm = await showYesNoDialog(
            context, 'You want to clear all memory and disk cache data?');
        if (confirm == true) {
          getIt<TimelineManager>().clear();
          getIt<IConnectionsRepo>().clear();
          getIt<NotificationsManager>().clear();
        }
      }),
      buildMenuButton('Logout', () async {
        final confirm = await showYesNoDialog(context, 'Log out account?');
        if (confirm == true) {
          await getIt<AuthService>().signOut();
        }
      }),
    ];
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridView(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              mainAxisExtent: menuButtonHeight,
              maxCrossAxisExtent: menuButtonWidth,
            ),
            children: menuItems,
          ),
        ),
      ),
      bottomNavigationBar: const AppBottomNavBar(
        currentButton: NavBarButtons.menu,
      ),
    );
  }

  Widget buildMenuButton(String title, Function() onPressed) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
        onPressed: onPressed,
        child: Text(title),
      ),
    );
  }
}
