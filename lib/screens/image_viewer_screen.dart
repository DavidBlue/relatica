import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

import '../globals.dart';
import '../models/media_attachment.dart';
import '../services/auth_service.dart';
import '../utils/snackbar_builder.dart';

class ImageViewerScreen extends StatelessWidget {
  final MediaAttachment attachment;

  const ImageViewerScreen({super.key, required this.attachment});

  Future<void> saveImage(BuildContext context) async {
    final appsDir = await getApplicationDocumentsDirectory();
    final filename = p.basename(attachment.fullFileUri.path);
    final bytesResult = await getIt<AuthService>()
        .currentClient
        .value
        .getFileBytes(attachment.uri);
    if (bytesResult.isFailure) {
      buildSnackbar(context,
          'Error getting full size version of file: ${bytesResult.error}');
    }
    if (Platform.isAndroid || Platform.isIOS) {
      final params = SaveFileDialogParams(
        data: bytesResult.value,
        fileName: filename,
      );
      await FlutterFileDialog.saveFile(params: params);
    } else {
      final location = await FilePicker.platform.saveFile(
        dialogTitle: 'Save Image',
        fileName: filename,
        initialDirectory: appsDir.path,
      );
      if (location != null) {
        await File(location).writeAsBytes(bytesResult.value);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () => saveImage(context),
              icon: const Icon(Icons.download))
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: width,
              height: 0.8 * height,
              child: InteractiveViewer(
                maxScale: 10.0,
                scaleFactor: 400,
                child: CachedNetworkImage(imageUrl: attachment.uri.toString()),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(attachment.description),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
