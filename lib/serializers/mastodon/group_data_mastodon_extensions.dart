import '../../models/group_data.dart';

extension GroupDataMastodonExtensions on GroupData {
  static GroupData fromJson(Map<String, dynamic> json) => GroupData(
        json['id'],
        json['title'],
      );
}
