import '../../models/hashtag.dart';

extension HashtagMastodonExtensions on Hashtag {
  static Hashtag fromJson(Map<String, dynamic> json) {
    final tag = json['name'];
    final url = json['url'];
    return Hashtag(
      tag: tag,
      url: url,
    );
  }
}
