import '../../globals.dart';
import '../../models/connection.dart';
import '../../services/auth_service.dart';

extension ConnectionMastodonExtensions on Connection {
  static Connection fromJson(Map<String, dynamic> json) {
    final name = json['display_name'] ?? '';
    final id = json['id'] ?? '';
    final profileUrl = Uri.parse(json['url'] ?? '');
    const network = 'Unknown';
    final avatar = Uri.tryParse(json['avatar_static'] ?? '') ?? Uri();
    final String handleFromJson = json['acct'];

    late final String handle;
    if (handleFromJson.contains('@')) {
      handle = handleFromJson;
    } else {
      final server = getIt<AuthService>().currentServer;
      handle = '$handleFromJson@$server';
    }

    return Connection(
      name: name,
      id: id,
      handle: handle,
      profileUrl: profileUrl.toString(),
      network: network,
      avatarUrl: avatar.toString(),
    );
  }
}
