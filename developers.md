# Relatica Developer Notes

Relatica is written with [Flutter](https://flutter.dev/). Please follow 
[the install instructions](https://docs.flutter.dev/get-started/install) for your platform
to get started with development. 

## Linux Development
For Linux development be sure that libsecret-1-dev and libjsoncpp-dev are installed on the machine.
Also the ObjectBox library requires a more recent version of CMake than what is packaged in the 
standard Flutter Snap install instructions. To get this working correctly on Linux you will 
have to do the 
[manual installation version of the Flutter install instructions](https://docs.flutter.dev/get-started/install/linux#install-flutter-manually).

## Additional Build Steps
Whenever a model is changed that is stored in ObjectBox it is necessary to execute the command:

```bash
flutter pub run build_runner build
```

Whenever building for deployment on a Linux machine you need to manually copy the libobjectbox.so 
library file into the bundle's lib folder before zipping up. This issue has been filed with the 
ObjectBox team to address: https://github.com/objectbox/objectbox-dart/issues/504

